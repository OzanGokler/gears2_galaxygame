﻿var GameUI = function () { };

GameUI.prototype = {
    
    preload: function()
    {
        game.add.sprite(0, 0, "bg");
    },

    create: function()
    {
        
        _ship.create();
        _controller.create();
        _enemies.createEnemy(15);
    },

    update: function()
    {        
        _ship.update();
        _enemies.update();
        _collisionHandler.update();        
    }
};

