﻿var CollisionHandler = function () { };

CollisionHandler.prototype = {
    hitPointEnemy: null,
    currentHitPointEnemy: null,
    ammo: false,
    touchEnemy: function (ship, enemy)
    {
        game.state.start(gameStates.gameover);
    },
    
    hitEnemy: function(bullet, enemy)
    {
        bullet.kill();
        console.log(this.ammo);
        if (this.ammo === true)
        {
            game.add.sprite(enemy.x, enemy.y, "ammo");
            
        }
        enemy.kill();
        
        _enemies.enemies.splice(this.hitPointEnemy, 1, new _enemies.enemy);
        _ship.skor += 1;
        console.log(_enemies.enemies);
    },

    checkAllCollision: function()
    {
        //hit enemies
        for (var i = 0; i < _enemies.enemies.length; i++)
        {
            this.hitPointEnemy = i;
            var collide = game.physics.arcade.collide(_ship.bullets, _enemies.enemies[i].enemySprite, this.hitEnemy, null, this);
            if (collide)
            {
                this.ammo = _enemies.enemies[i].ammo;
            }
        }

        //crash enemies
        for (var j = 0; j < _enemies.enemies.length; j++)
        {
            
            game.physics.arcade.collide(_ship.myShip, _enemies.enemies[j].enemySprite, this.touchEnemy, null, this);
        }
    },

    update: function()
    {
        this.checkAllCollision();
    }

}

var _collisionHandler = new CollisionHandler();