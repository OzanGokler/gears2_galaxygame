﻿var Enemies = function () { };

Enemies.prototype = {
    x: null,
    y: null,
    speed: null,
    maxX: 860,
    minX: -500,
    enemySprite: null,
    enemies: new Array(),
    comingArea1: null,
    comingArea2: null,
    comingArea3: null,
    comingArea4: null,
    ammo: false,
    
    enemy: function()
    {
        var comingArea = _utils.getRandomIntBetween(1, 4);
        console.log("gelen bolge = " + comingArea);
        switch (comingArea)
        {
            case 1:
                this.x = _utils.getRandomIntBetween(-500, 860);
                this.y = -500;
                break;
            case 2:
                this.x = 860;
                this.y = _utils.getRandomIntBetween(-500, 860);
                break;
            case 3:
                this.x = _utils.getRandomIntBetween(-500, 860);
                this.y = 860;
                break;
            case 4:
                this.x = -500;
                this.y = _utils.getRandomIntBetween(-500, 860);
                break;
        }
        this.ammo = false;
        this.speed = _utils.getRandomIntBetween(1, 120);
        this.enemySprite = game.add.sprite(this.x, this.y, "enemy");
        game.physics.enable(this.enemySprite);
        this.enemySprite.anchor.setTo(0.5, 0.5);
        //this.enemySprite.body.collideWorldBounds = true;
        this.enemySprite.body.bounce.setTo(1, 1);
        this.enemySprite.body.immovable = true;
        
    },

    createEnemy: function(numbers)
    {       
        for (var i = 0; i < numbers; i++)
        {
            this.enemies.push(new this.enemy());
        }
        this.setAmmo();
    },

    setAmmo: function()
    {      
        for (var i = 0; i < _enemies.enemies.length; i++)
        {
            console.log("ahanda " + this.enemies.ammo);
            var rand = _utils.getRandomIntBetween(1, 4);
            if (this.enemies[i].ammo === false)
            {
                this.enemies[i].ammo = true;
            }          
        }
    },

    move: function()
    {
        for (var i = 0; i < this.enemies.length; i++)
        {           
            game.physics.arcade.moveToObject(this.enemies[i].enemySprite, _ship.myShip, this.enemies[i].speed);
        }            
    },

    update: function()
    {
        this.move();      
    }
};
var _enemies = new Enemies();