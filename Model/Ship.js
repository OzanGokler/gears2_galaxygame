﻿var Ship = function () { };

Ship.prototype = {
    myShip: null,
    bullets: null,
    bulletTime: 0,
    skor: 0,
 
    create: function()
    {
        this.myShip = game.add.sprite(game.width / 2, game.height / 2, "player");
        this.myShip.anchor.setTo(0.5,0.5);
        this.myShip.enableBody = true;
        game.physics.enable(this.myShip, Phaser.Physics.ARCADE);

        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(30, "bullet");
        this.bullets.setAll("anchor.x", 0.5);
        this.bullets.setAll("anchor.y", 0.5);
        this.bullets.setAll("outOfBoundsKill", true);
        this.bullets.setAll("checkWorldBounds", true);

        game.world.bringToTop(this.myShip);
    },
    
    fireBullets: function(fireTime,bulletSpeed)
    {
        if (game.time.now > this.bulletTime) {
            //  Grab the first bullet we can from the pool
           var bullet = this.bullets.getFirstExists(false);
           var bulletAngle = this.myShip.angle;
           bullet.anchor.setTo(0.5);
           //if(_enemies.ammo === true)
           if (bullet)
           {
                // And fire it
                bullet.reset(this.myShip.x, this.myShip.y);
                bullet.velocity = game.physics.arcade.velocityFromAngle(bulletAngle, bulletSpeed, bullet.body.velocity);
                this.bulletTime = game.time.now + fireTime;
            }
        }
    },



    update: function()
    {
        this.fireBullets(500, 200);        
    }
}

var _ship = new Ship();