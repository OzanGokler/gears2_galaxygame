﻿var LoadingUI = function () { };

LoadingUI.prototype = {
    loadAssets: function()
    {
        game.load.image("bg", "Assets/bg.jpg");
        game.load.image("player", "Assets/player.png");
        game.load.image("enemy", "Assets/enemy.png");
        game.load.image("bullet", "Assets/bullet.png");
        game.load.image("ammo", "Assets/ammo.png");
        game.load.image("gameover", "Assets/gameover.jpg");
    },

    loadScripts: function()
    {
        game.load.script("gameover", "View/GameOver.js");
        game.load.script("gameUI", "Model/GameUI.js");       
        game.load.script("utils", "Model/Utils.js");
        game.load.script("controller", "Model/Controller.js");
        game.load.script("ship", "Model/Ship.js");
        game.load.script("enemies", "Model/Enemies.js");
        game.load.script("collisionHandler", "Model/CollisionHandler.js");
    },

    StartGameStates: function ()
    {
        game.state.add(gameStates.main, GameUI);
        game.state.add(gameStates.gameover, GameOver);
        game.state.start(gameStates.main);
    },

    preload: function()
    {
        this.loadScripts();
        this.loadAssets();
    },

    create: function()
    {
        this.StartGameStates();
    }
};