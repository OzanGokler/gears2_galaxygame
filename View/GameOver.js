﻿var GameOver = function () { };

GameOver.prototype = {
    gameOverButton: null,
    skorText: null,
    style: { font: "24px Helvetica", fill: "#ffffff", wordWrap: false },

    preload: function () {
        game.add.sprite(0, 0, "bg");
        this.gameOverButton = game.add.sprite(game.width / 2, game.height / 2, "gameover");
        this.gameOverButton.anchor.setTo(0.5);
        this.gameOverButton.scale.setTo(0.7);
        this.gameOverButton.inputEnabled = true;
        this.skorText = game.add.text(0, 0, "SKOR: " + _ship.skor.toString(), this.style);

    },

    create: function ()
    {
        this.gameOverButton.events.onInputDown.add(this.backToGame, this);
    },

    backToGame: function()
    {
        game.state.start(gameStates.loading);
    }
};