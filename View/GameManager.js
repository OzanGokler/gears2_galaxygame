﻿var gameWidth = 360;
var gameHeight = 360;
var backgroundColor = "#000000";
var game = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, "GearGame"
    /*{ preload: preload, create: create, update: update }*/);
var gameStates = { main: "GameUI", loading: "Loading", gameover: "GameOver"};


Main = function () { };

Main.prototype = {

    preload: function ()
    {
        game.load.script(gameStates.loading, "View/Loading.js");
    },

    create: function ()
    {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.state.add(gameStates.loading, LoadingUI);
        game.state.start(gameStates.loading);        
    }
};
game.state.add("Main", Main);
game.state.start("Main");
